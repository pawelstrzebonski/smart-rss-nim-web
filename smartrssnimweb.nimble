# Package

version       = "0.1.0"
author        = "Pawel Strzebonski"
description   = "A machine learning enhanced RSS feed manager with a web-server back-end and a web-browser front-end, written in Nim."
license       = "AGPL-3.0-or-later"
srcDir        = "src"
bin           = @["smartrssnimweb"]


# Dependencies

requires "nim >= 1.6.8"

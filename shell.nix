{ pkgs ? import <nixpkgs> {} }:
with pkgs;
mkShell rec {
    name = "shell";
    buildInputs = [
        nim2 openssl
    ];
}

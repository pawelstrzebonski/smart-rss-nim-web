{ pkgs ? import <nixpkgs> {} }:
with pkgs;

buildNimPackage rec {
  name = "smartrssnimweb";
  buildInputs = [ openssl ];
  rev = "baff673453089bc127ac8427e313a7523caae661";
  src = fetchFromGitLab {
    inherit rev;
    owner = "pawelstrzebonski";
    repo = "smart-rss-nim-web";
    sha256 = "sha256-Hv2z2fz6Rx+iuB0Uau312EafG2n7Ptd8xIUQ3gS7b4g=";
  };
  nimFlags = ["-d:ssl"];
  meta = with lib; {
    description = "A machine learning enhanced RSS feed manager with a web-server back-end and a web-browser front-end, written in Nim.";
    homepage = "https://gitlab.com/pawelstrzebonski/smart-rss-nim-web";
    license = licenses.agpl3Plus;
    #maintainers = with maintainers; [ pawelstrzebonski ];
    platforms = platforms.linux;
  };
}

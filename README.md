# Smart-RSS Nim Web

A machine learning enhanced RSS feed manager (written in Nim). Another in my series of programming practice projects on RSS feed managers including [Smart-RSS Go Web](https://gitlab.com/pawelstrzebonski/smart-rss-go-web) and [Rusty Smart-RSS Web](https://gitlab.com/pawelstrzebonski/rusty-smart-rss-web), among others.

For more information about program design/implementation, please consult the [documentation pages](https://pawelstrzebonski.gitlab.io/smart-rss-go-web/).

## Features

* Automatic periodic RSS feed checking
* Machine learning scoring and ordered presentation of feed items
* Local database and processing
* Web UI, use the web browser of your choice
* No JavaScript

## Screenshots

![Main web UI](docs/img/webui.png)

## Usage/Installation

This repository contains the Nimble project at top level with Nim source code in the `src/` directory. It can be built using `nimble build`.

If you have [Nix](https://nixos.org/) then you can use the included `shell.nix` to start a shell with installed dependencies for development, or else you can use the `default.nix` script to build/install a specific version of this application. Note that `default.nix` specifies the commit to be built, so it may need to be updated to build the latest version.

Once built, run the `smartrssnimweb` binary to start the back-end server. To access the UI, open in your browser of choice `localhost:8090` (this may be different depending on platform). The application takes command-line arguments `-port` (port number on which to serve this application) and `-db $DATABASEFILENAME` to define the database to create/use by filename.

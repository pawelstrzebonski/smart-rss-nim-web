# Smart-RSS Nim Web

Smart-RSS Nim Web is a machine learning enhanced RSS feed manager, written in Nim, with a back-end web-server and web-UI front-end.

## Features

* Automatic periodic RSS feed checking
* Machine learning scoring and ordered presentation of feed items
* Local database and processing
* Web UI, use the web browser of your choice
* No JavaScript

## Screenshots

![Main web UI](img/webui.png)

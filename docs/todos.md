# TODOs

This project is by no means finished/polished. A the following is a shopping list of improvements (in no particular order):

* Better UI:
	* expose more application settings
	* icons?
	* count # of unread items
	* nicer presentation
* Better handling of non-ASCII/international content
	* internationalization of templates
* General code/project improvements:
	* tests
	* CI
	* build documentation from source
	* better data serialization
	* allow for user-defined template/HTML?

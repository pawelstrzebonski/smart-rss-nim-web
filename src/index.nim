import std/htmlgen
import types

func makeIndex*(feeds: seq[string], items: seq[Item]): string =
    ## Generate HTML of app's main page
    let numItemsPerPage = 4
    let html1 = head(
        meta(charset = "UTF-8"),
        meta(name = "Author", content = "Pawel Strzebonski"),
        meta(name = "Description", content = "Smart-RSS Nim Web: A machine learning enhanced RSS feed manager."),
        title("Smart-RSS Nim Web"),
        #{{/* Hack to avoid reloading page on certain form submissions */}}
        style(media = "screen", """
iframe{
  display: none;
}
body{
    background-color: goldenrod;
}"""),
    )
    var feedslist = ""
    for feed in feeds:
        feedslist&=option(value = feed, feed)
    var itemslist = ""
    for item in items:
        itemslist&="(Score: "&($item.score)&") - "&b(item.title)&" - "&i(item.summary)
        #{{/* Bottons for interacting with item */}}
        itemslist&=form(`method` = "post", action = "/",
            input(type = "hidden", id = "url{{$i}}", name = "url",
                    value = item.url),
            button(type = "submit", name = "action", formaction = "/like",
                    formtarget = "frame", "Like"),
            button(type = "submit", name = "action", formaction = "/dislike",
                    formtarget = "frame", "Dislike"),
            button(type = "submit", name = "action", formaction = "/open",
                    formtarget = "_blank", "Open"),
            )&hr()

    let html2 = body(
    h1("RSS Items"),
    #{{/* Box for adding in a new feed */}}
    form(action = "/nextpage", `method` = "post",
    button(type = "submit", name = "action", "Next Page")),
    #{{/* List all RSS items */}}
    hr(),
    itemslist,
    #{{/* Box for adding in a new feed */}}
    form(action = "/nextpage", `method` = "post",
    button(type = "submit", name = "action", "Next Page")),
    h1("Settings"),
    hr(),
    #{{/* Box for setting # of feeds per page */}}
    form(action = "/setitemnum", `method` = "post",
    p(
    label(`for` = "numitems", "# of items per page:"),
    input(type = "number", id = "numitems", name = "num",
            value = $numItemsPerPage),
    input(type = "submit", value = "Set", formtarget = "_self"),
    input(type = "reset")
    )),
    h2("Add a new feed:"),
    #{{/* Box for adding in a new feed */}}
    form(action = "/add", `method` = "post",
    p(
    label(`for` = "urladd", "Feed URL:"),
    input(type = "text", id = "urladd", name = "url"),
    label(`for` = "numupdateinterval", "Update interval (seconds):"),
    input(type = "number", id = "numupdateinterval", name = "num",
            value = $3600),
    input(type = "submit", value = "Add", formtarget = "_self"),
    input(type = "reset"),
    )
    ),
    h2("Remove a feed:"),
    #{{/* Box for removing a feed */}}
    form(action = "/remove", `method` = "post",
    label(`for` = "urlremove", "Feed URL:"),
    select(id = "urlremove", name = "url",
    feedslist
    ),
    input(type = "submit", value = "Remove", formtarget = "_self"),
    ),
    #{{/* Box for terminating program */}}
    form(action = "/quit", `method` = "post",
    button(type = "submit", name = "action", "Quit (will terminate server)")),
    iframe(name = "frame")
    )
    return html1&html2

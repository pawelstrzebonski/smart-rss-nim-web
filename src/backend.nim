import std/[xmlparser, xmltree, streams, tables, marshal, times, enumerate,
        algorithm, httpclient, os]
import types
import classifier

proc getFeed*(url: string): seq[Item] =
    ## Download the feed and return list of feed items
    var client = newHttpClient()
    let s = client.getContent(url)
    # Parse the feed into XML tree
    let tree = parseXml(newStringStream(s))
    # Sift through the tree for feed items
    var items: seq[Item]
    var entries = newSeq[XmlNode]()
    # Handle Atom feeds
    if tree.tag == "feed":
        tree.findAll("entry", entries)
        for entry in entries:
            let url = entry.child("link").attr("href")
            let title = sanitize(entry.child("title").innerText)
            let summary = sanitize(entry.child("content").innerText)
            items.add(Item(title: title, summary: summary, url: url, score: 0,
                    rating: 0, isOpened: false, isSeen: false))
    # Handle RSS feeds
    elif tree.tag == "rss":
        tree.findAll("item", entries)
        for entry in entries:
            let url = entry.child("link").innerText
            let title = sanitize(entry.child("title").innerText)
            let summary = sanitize(entry.child("description").innerText)
            items.add(Item(title: title, summary: summary, url: url, score: 0,
            rating: 0, isOpened: false,
            isSeen: false))
    return items

func makeBackend*(filename: string): Backend =
    ## Create backend object
    var be = Backend(
    itemstore: initTable[string, Item](),
    feedstore: initTable[string, Feed](),
    itemlist: newSeq[ScoredItem](),
    ismodified: true,
    filename: filename,
    classifier: newPredictor())
    return be

proc addItem*(be: var Backend, item: var Item) =
    ## Add item to backend datastore
    if be.itemstore.hasKey(item.url):
        #Do not add, already in store
        discard
    else:
        let score_table = be.classifier.score(item2string(item))
        let score = score_table.getOrDefault("opened",
                0)+score_table.getOrDefault("liked",
                0)+score_table.getOrDefault("disliked", 0)
        item.score = score
        be.itemstore[item.url] = item
        be.itemlist.add(ScoredItem(url: item.url, score: score))
        be.ismodified = true

proc addFeed*(be: var Backend, url: string, updateperiod: Duration) =
    ## Add feed of given url and desired update period to backend datastore
    if be.feedstore.hasKey(url):
        echo("Not adding ", url, ", already exists.")
    else:
        echo("Adding new feed ", url, "...")
        var newitems = getFeed(url)
        let newfeed = Feed(url: url, lastupdate: now(),
                updateperiod: updateperiod)
        be.feedstore[url] = newfeed
        for item in mitems(newitems):
            be.addItem(item)
        echo("Added new feed ", url)
        be.itemlist.sort(cmp)
        be.ismodified = true

proc removeFeed*(be: var Backend, url: string) =
    ## Remove feed to backend datastore
    if not be.feedstore.hasKey(url):
        echo("Not removing ", url, ", does not exist.")
    else:
        echo("Removing feed ", url, "...")
        be.feedstore.del(url)
        echo("Removed feed ", url)
        be.ismodified = true

proc updateFeeds*(be: var Backend) =
    ## Check if feeds are due for an update, and if so, update
    let n = now()
    # Iterate over all feeds
    for url, feed in be.feedstore.mpairs:
        # Check if feed is out-of-date
        if n-feed.lastupdate > feed.updateperiod:
            # If so, fetch and add new items
            var items = getFeed(url)
            for item in mitems(items):
                be.addItem(item)
            # Bump update time
            feed.lastupdate = n
            be.ismodified = true
    # Update sorting of items
    if be.ismodified:
        be.itemlist.sort(cmp)

func getUnreadItems*(be: Backend, limit: int): seq[Item] =
    ## Return (up to defined limit) a list of unread feed items
    var outlist: seq[Item]
    if be.itemlist.high < 0:
        return outlist
    for si in be.itemlist[be.itemlist.low..<min(limit, be.itemlist.high)]:
        outlist.add(be.itemstore[si.url])
    return outlist

proc markOpened*(be: var Backend, url: string) =
    ## Mark item (defined by url) as opened
    var item = be.itemstore[url]
    item.isOpened = true
    be.itemstore[url] = item
    be.ismodified = true

proc like*(be: var Backend, url: string) =
    ## Mark item (defined by url) as liked
    var item = be.itemstore[url]
    item.rating = 1
    be.itemstore[url] = item
    be.ismodified = true

proc dislike*(be: var Backend, url: string) =
    ## Mark item (defined by url) as disliked
    var item = be.itemstore[url]
    item.rating = -1
    be.itemstore[url] = item
    be.ismodified = true

proc markSeen*(be: var Backend, url: string) =
    ## Mark item (defined by url) as seen
    var item = be.itemstore[url]
    item.isSeen = true
    be.itemstore[url] = item
    for idx, item in enumerate(be.itemlist):
        if item.url == url:
            be.itemlist.delete(idx)
            var classes: seq[string]
            if be.itemstore[url].isOpened:
                classes.add("opened")
            if be.itemstore[url].rating > 0:
                classes.add("liked")
            elif be.itemstore[url].rating < 0:
                classes.add("disliked")
            be.classifier.addDocument(be.itemstore[url].item2string(), classes)
            be.ismodified = true
            break

proc saveBackend*(be: var Backend) =
    ## If backend has been modified, save it.
    if be.ismodified:
        echo("Writing to file...")
        be.ismodified = false
        #TODO: use a better (binary) format for serialization
        writeFile(be.filename, $$be)
        echo("Written to file.")
    else:
        echo("Not modified, not writing to file.")

proc loadBackend*(filename: string): Backend =
    ## Load backend from file
    let data = readFile(filename)
    #TODO: use a better (binary) format for serialization
    let be = to[Backend](data)
    return be

proc setupBackend*(filename: string): Backend =
    ## Load backend from file or create it if it does not exist
    if fileExists(filename):
        return loadBackend(filename)
    else:
        return makeBackend(filename)

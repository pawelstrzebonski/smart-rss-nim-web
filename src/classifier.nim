import std/[tables, math, strutils]

type
    Predictor* = object                ## Naive-Bayes/TF-IDF scoring object
        total_word_count: int          ## Total # of words encountered
        total_document_count: int      ## Total # of documents encountered
        documents_per_class: Table[string, int] ## Total # of documents encountered in a class
        documents_per_class_word: Table[(string, string),
                int] ## Total # of documents of a certain class encountered containing a word
        documents_per_word: Table[string,
                int]                   ## Total # of documents encountered containing a word
        word_count: Table[string, int] ## Total # of word encounters
        word_count_per_class: Table[string, int] ## Total # of words encountered in class
        word_count_per_class_word: Table[(string, string), int] ## Per-class/word count

#https://en.wikipedia.org/wiki/Tf%E2%80%93idf

func countWords(doc: openArray[string]): Table[string, int] =
    ## Count how many times each word appears
    var wc = initTable[string, int]()
    for word in doc:
        wc[word] = wc.getOrDefault(word, 0)+1
    return wc

func doc2words(doc: string): seq[string] =
    ## Split a document string into list of words
    return split(doc, ' ')

func newPredictor*(): Predictor =
    ## Create new, empty text class predictor
    return Predictor(
        total_word_count: 0,
        total_document_count: 0,
        documents_per_class: initTable[string, int](),
        documents_per_class_word: initTable[(string, string), int](),
        documents_per_word: initTable[string, int](),
        word_count: initTable[string, int](),
        word_count_per_class: initTable[string, int](),
        word_count_per_class_word: initTable[(string, string), int](),
    )

func addDocument*(p: var Predictor, document: string, classes: openArray[string]) =
    ## Add document into `Predictor` collection under specified classes
    let words = doc2words(document)
    let wc = countWords(words)
    # Count this document
    p.total_document_count+=1
    for class in classes:
        p.documents_per_class[class] = p.documents_per_class.getOrDefault(class, 0)+1
        p.word_count_per_class[class] = p.word_count_per_class.getOrDefault(
                class, 0)+len(words)
    # Count the words in document
    for word, count in wc.pairs:
        p.documents_per_word[word] = p.documents_per_word.getOrDefault(word, 0)+1
        for class in classes:
            p.documents_per_class_word[(class,
                    word)] = p.documents_per_class_word.getOrDefault((class,
                    word), 0)+1
            p.word_count_per_class_word[(class,
                    word)] = p.documents_per_class_word.getOrDefault((class,
                    word), 0)+count
        p.word_count[word] = p.word_count.getOrDefault(word, 0)+count
    p.total_word_count+=len(words)

func tfidf(p: Predictor, document: string): Table[string, float] =
    ## Calculate TF-IDF score of each word in document
    let words = doc2words(document)
    # Count the words in document
    let wc = countWords(words)
    # Word (term) frequency within document
    var tf = initTable[string, float]()
    for word, count in wc.pairs:
        tf[word] = count/len(words)
    # Inverse-[frequency of word in documents]
    var idf = initTable[string, float]()
    for word, _ in wc.pairs:
        idf[word] = log(p.total_document_count/(
                1+p.documents_per_word.getOrDefault(word, 0)),
                10) #TODO: what base is appropriate here?
    # Combine TF and IDF into single quantity
    var tfidf = initTable[string, float]()
    for word, _ in tf.pairs:
        tfidf[word] = tf[word]*idf[word]
    return tfidf

func naivebayes(p: Predictor, document: string): Table[(string, string), float] =
    ## Calculate per-class/word Bayesian probabilities
    # Count the words in document
    let wc = countWords(doc2words(document))
    # Probability of being a particular class
    var p_class = initTable[string, float]()
    for class, count in p.documents_per_class.pairs:
        p_class[class] = count/p.total_document_count
    # Probability of encountering a particular word
    var p_word = initTable[string, float]()
    for word, _ in wc.pairs:
        p_word[word] = p.word_count.getOrDefault(word, 0)/p.total_word_count
    # Probability of encountering a given word, per class
    var p_word_given_class = initTable[(string, string), float]()
    for word, _ in wc.pairs:
        for class, _ in p.documents_per_class.pairs:
            p_word_given_class[(class, word)] = p.word_count_per_class_word.getOrDefault(
                    (class, word), 0)/p.word_count_per_class.getOrDefault(class, 1)
    # Calculate per-word Bayesian probability
    var p_class_given_word = initTable[(string, string), float]()
    for key, _ in p_word_given_class.pairs:
        let
            class = key[0]
            word = key[1]
        p_class_given_word[(class, word)] = p_class[class]*p_word_given_class[(
                class, word)]/p_word[word]
    return p_class_given_word

func score*(p: Predictor, document: string): Table[string, float] =
    ## Calculate a per-class TF-IDF/Naive-Bayes hybrid score
    # Get per-word TF-IDF scores
    let tfidf = p.tfidf(document)
    # Get per-class/word Naive Bayes scores
    let nb = p.naivebayes(document)
    var scores = initTable[string, float]()
    for class, _ in p.documents_per_class.pairs:
        var class_score = 0.0
        for word, word_score in tfidf.pairs:
            let nb_score = nb[(class, word)]
            if nb_score > 0:
                class_score+=word_score*log(nb_score, 10)
        scores[class] = class_score
    return scores


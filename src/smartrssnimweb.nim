import std/[asynchttpserver, asyncdispatch, times, tables, strutils, uri, os, parseopt]
import backend
import index
import types

const FAVICON = staticRead("favicon.ico") ## Embed favicon into binary
const UPDATEPERIOD = 1_000*60*10 ## Update checking period in ms
let APPDIR = joinPath(getConfigDir(), "smartrssnim") ## App data directory
discard existsOrCreateDir(APPDIR)

var port = 8090
var savefile = joinPath(APPDIR, "smartrssnim.bin")

# Parse command line arguments
var p = initOptParser()
while true:
  p.next()
  case p.kind
  of cmdEnd: break
  of cmdShortOption, cmdLongOption:
    if p.val == "":
      echo "Unknow option: ", p.key
    else:
      case p.key:
        of "db":
          savefile = joinPath(APPDIR, p.val)
        of "port":
          port = parseInt(p.val)
        else:
          echo "Unkown option/value: ", p.key, ", ", p.val
  of cmdArgument:
    echo "Unused argument: ", p.key

func parseBody(body: string): Table[string, string] =
  ## Parse HTTP request body into table
  var output = initTable[string, string]()
  for kvpair in body.split('&'):
    let res = kvpair.split('=', 1)
    output[res[0]] = decodeUrl(res[1])
  return output

proc main {.async.} =
  ## Main server function
  var be = setupBackend(savefile)
  var lastupdate = now()
  var server = newAsyncHttpServer()
  var itemsPerPage = 4
  var shownItems: seq[Item]
  proc cb(req: Request) {.async.} =
    ## Request handler
    case req.reqMethod:
      of HttpGet:
        case req.url.path:
          of "/":
            let headers = {"Content-type": "text/html; charset=utf-8"}
            var feeds: seq[string]
            for url in be.feedstore.keys:
              feeds.add(url)
            shownItems = be.getUnreadItems(itemsPerPage)
            let response = makeIndex(feeds = feeds,
                    items = shownItems)
            await req.respond(Http200, response,
                    headers.newHttpHeaders())
          of "/favicon.ico":
            let headers = {"Content-type": "image"}
            await req.respond(Http200, FAVICON,
                    headers.newHttpHeaders())
          else:
            echo "Asking for non-existent ", req.url
            await req.respond(Http404, "")
      of HttpPost:
        case req.url.path:
          of "/add":
            var body = parseBody(req.body)
            if body.hasKey("url") and body.hasKey("num"):
              let headers = {"Location": "/"}
              be.addFeed(body["url"], initDuration(
                      seconds = parseInt(body["num"])))
              await req.respond(Http302, "OK",
                      headers.newHttpHeaders())
          of "/remove":
            var body = parseBody(req.body)
            if body.hasKey("url"):
              let headers = {"Location": "/"}
              be.removeFeed(body["url"])
              await req.respond(Http302, "OK",
                      headers.newHttpHeaders())
          of "/nextpage":
            for item in shownItems:
              be.markSeen(item.url)
            let headers = {"Location": "/"}
            await req.respond(Http302, "OK", headers.newHttpHeaders())
          of "/like":
            var body = parseBody(req.body)
            if body.hasKey("url"):
              be.like(body["url"])
              await req.respond(Http200, "OK")
          of "/dislike":
            var body = parseBody(req.body)
            if body.hasKey("url"):
              be.dislike(body["url"])
              await req.respond(Http200, "OK")
          of "/open":
            var body = parseBody(req.body)
            if body.hasKey("url"):
              be.markOpened(body["url"])
              let headers = {"Location": body["url"]}
              be.markOpened(body["url"])
              await req.respond(Http302, "OK",
                      headers.newHttpHeaders())
          of "/setitemnum":
            var body = parseBody(req.body)
            if body.hasKey("num"):
              itemsPerPage = parseInt(body["num"])
              let headers = {"Location": "/"}
              await req.respond(Http302, "OK",
                      headers.newHttpHeaders())
          of "/quit":
            echo "Recieved exit command..."
            be.saveBackend()
            quit()
          else:
            echo "Asking for non-existent ", req.url
            await req.respond(Http404, "")
      else:
        await req.respond(Http405, "")

  #TODO: autosave on program close?

  proc update() {.async.} =
    ## Periodic update/save function
    while true:
      be.updateFeeds()
      be.saveBackend()
      await sleepAsync(UPDATEPERIOD)
  asyncCheck update()

  server.listen(Port(port))
  let port = server.getPort
  while true:
    if server.shouldAcceptRequest():
      await server.acceptRequest(cb)
    else:
      # too many concurrent connections, `maxFDs` exceeded
      # wait 500ms for FDs to be closed
      await sleepAsync(500)

waitFor main()

import std/[tables, times, re, strutils]
import classifier

type
    Item* = object       ## RSS feed item object
        title*: string   ## Title of item
        summary*: string ## Summary text of item
        url*: string     ## URL of item
        score*: float    ## Classifiers score based on item text
        isSeen*: bool    ## Has item been shown to user?
        rating*: int     ## User's rating of item (+1 is like, -1 is dislike, 0 is no data)
        isOpened*: bool  ## Has user opened this item in browser?
        #TODO: add tags, lastupdate?

type
    Feed* = object              ## RSS feed object
        url*: string            ## Feed URL
        lastupdate*: DateTime   ## DateTime of last feed update
        updateperiod*: Duration ## Desired period between feed updates

type
    ScoredItem* = object ## Scored feed item
        url*: string     ## URL of item (used to find item in items datastore)
        score*: float    ## Classifier's score for this item

proc cmp*(x, y: ScoredItem): int =
    ## Compare scores of scored items
    cmp(y.score, x.score)

type
    Backend* = object                   ## Smart-RSS backend object
        itemstore*: Table[string, Item] ## Datastore of RSS items
        feedstore*: Table[string, Feed] ## Datastore of RSS feeds
        itemlist*: seq[ScoredItem]      ## List of scored/sorted items
        ismodified*: bool               ## Is backend modified (vs save file)
        filename*: string               ## Save filename
        classifier*: Predictor          ## Text classifier for score calculation

func sanitize*(s: string): string =
    ## Sanitize a string to be mostly alphanumeric
    return toLowerAscii(multiReplace(s, [(re"&.*?;", " "),
    (re"<.*?>", " "), (re"[\n\t]", " ")]))

func item2string*(i: Item): string =
    ## Convert item into string of words for classification
    #TODO: handle tags when added?
    return sanitize(i.title)&' '&sanitize(i.summary)
